# Pad Zabbix


 sudo virt-builder debian-11  \
  -o /opt/vm/debian11_base.qcow2 \ 
  --size 20G \
  --format qcow2  \ 
  --root-password password:pirineus  \
    --update \
    --install sudo,network-manager,tmux,vim,python3-pip,git,htop,firewalld,fail2ban  \
    --edit '/etc/default/keyboard: s/^XKBLAYOUT=.*/XKBLAYOUT="es"/'

## USERS AND PASSWORDS
       The --root-password option is used to change the root password (otherwise a random password is used).  This option
       takes a password "SELECTOR" in one of the following formats:

       --root-password file:FILENAME
           Read the root password from "FILENAME".  The whole first line of this file is the replacement password.  Any other
           lines are ignored.  You should create the file with mode 0600 to ensure no one else can read it.

       --root-password password:PASSWORD
           Set the root password to the literal string "PASSWORD".

           Note: this is not secure since any user on the same machine can see the cleartext password using ps(1). 



**du -d 1  -h /var/lib/docker** => direcotri  on vull fer la consulta
- **-h**  => human readable que els bytes son una mica chungos de llegir
- **-d 1**=>  depth nivell de profunditat dels diretoris
           
        
### Virsh :
- **define** => es guarda la definició de la màquina virtual a /etc i quan fem un virsh list --all apareix:
 - **create path.xml** => es crea la vm pero no es queda al /etc i no la puc llistar => seria com una vm temporal
 - **create path.xml** => es crea la vm pero no es queda al /etc i no la puc llistar => seria com una vm temporal
- **undefine** => es treu la definició de vm del /etc i ja no la llistem
- **start** => arrenca una vm definida al sistema
- **shutdown** => apagada ordenada
- **destroy** => a saco
- **reset** => reset a saco

    


## Demanar una ip per dhcp

        
`virsh net-list --all
sudo virsh net-autostart default
sudo virsh net-start default
sudo virsh create /opt/vm/debian_test.xml
sudo virsh list
sudo virsh console debian_test`

`nmcli connection add enp1s0  ipv4.method manual ipv4.addresses "192.168.122.10/24" connection.auto
connect yes`


https://docs.docker.com/engine/install/debian/

`sed -i -e 's|[#]*PermitRootLogin prohibit-password|PermitRootLog'                            in yes|g' /etc/ssh/sshd_config`

--------------------

Installar zabbix: https://gitlab.com/isard/welcome/-/blob/main/zabbix_install.md

--------------------

## Una vegada dins del servidor:

* apt install sudo
* apt install network-manager
* apt update
* systemctly status NetworkManager
* nmtui -> *textual user interface network manager*
    - sudo grep -R nmcli -> *per buscar coses de markdown*
* dhclient:
    - dhclient -r -> *alliberar client*
* ip r s -> *per coneixer el gateway*
* cat /etc/resolv.conf -> *per conèixer la meva dns*
* instal·lar docker