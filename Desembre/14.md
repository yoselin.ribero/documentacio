14 Desembre

# Màquines Virtuals

- **libvirtd.socket** -> fitxers que fa servir els processos per comunicar-se entre ells

- **sudo systemctl status libvirtd** -> per veure l'estat
- **virsh list** -> per veure lla lista
- **sudo systemctl stop libvirtd** -> per aturar tot
- **sudo systemctl disable libvirtd.socket ...(tots els sockets)** -> per a que no s'inici la següent vegada.
- **sudo systemctl start libvirtd** -> per iniciar-ho tot un altre vegada
- **qemu-img info (nomdeldisc)** -> per veure informació del disc, en el exemple fet, hem utilitzat un *qemu-img info debian10.qcow2*

### Creació de discos incrementals:

Exemple:

- **qemu-img create -f qcow2 -F qcow2 -b debian10.qcow2 child.qcow2**
    - -f: per dirl-li el format
    - -F: el format que vols posar-li
    - -b debian10.qcow2 child.qcow2: que fagi un servidor a partir del primer

- **qemu-img info child.qcow2**: per veure l'info d'aquest arxiu
    - tot el que afegeixes de nou, s'afegeix al disc de child
    - Adventatges: per poder probar coses ja que creas una màquina virtual igual al debian10 que ja tenies.

- **virt-copy-in** -> copia els fitxers que vull tenir dins de la màquina virtual
- **guestfish** -> et permet executar una màquina virtual sobre el disc dur que vulguis i pot executar el que sigui sense arrencar la màquina
- **ln -s** -> enllaç simbòlic d'un lloc a un altre

Es por fer també per la app virtual-maquina. Pots afegir xarxes virtuals noves apart de la de default.

El **drbd** et permet crear una replica de un servidor, és a dir, replica el disc dur. 





# MTU
**MTU 1500** -> buscar més info
- **ip link set #ip-xarxa mtu #que-vulguis-tamany** -> serveix per canviar la mtu
    - Té el problema que per defecte tots estan a 1500. Si incrementes el mtu, tens el peligre de que amb altres màquines no s'acaben de comunicar ja que l'altre màquina estará a 1500 i tu per exemple a 9000.

En el virt-maquina, pot afegir la mtu que vulguis en la pestanya de xarxes virtuals amb el comande;

```json
{
    <mtu size = "el-tamany-que-vulguis"/>
}
```
En les xarxes wifi, és normal que hi hagi molts ''dropped'' en paquets rebuts (RX).


- **sudo ifconfig** -> per veure el que tens connectat 
    - *Si no pots fer sudo ifconfig, tens que instal·lar el paquet net-tools (sudo apt install net-tools)*


- **sudo ethtool ID-de-la-xarxa** -> t'ensenya tot el que tinguis, per exemple quins cables utilitzes, etc.
- **sudo ethtool -S ID-de-la-xarxa** -> es veu on s'esta produïnt els errors.

## Per crear una mtu amb virsh

```json
{
    virsh net-define nets/drbd-net.xml
    virsh net-autostart drbd
    virsh net-start drbd
}
```
*drbd-net.xml : aquí afegeixes el paràmetres que vulguis.*

- **./create-cluster-drbd9.sh 2** -> això és per crear 2 clusters
- **$pwd** -> et retorna al directori on estas

**Important: Si creas els clusters com root, has de accedir com tal al vm, entras al cmd, i desde root de cmd fas un virt-manager**


# Tema Wireguard/servidor

- **wg show** -> et mostra els servidors
- **.cat /etc/wireguard/wg0.conf** -> per poder configurar el wireguard

<dl>
  <dt>* Ports ->  ___:___</dt>
  <dd>El primer és el host, el segon és on vull que vagi</dd>

</dl>

Pots buscar en el github qualsevol cosa, i en el seus tags, pots anar a assets, copies l'enllaç i s'instal·la.
1. **mkdir ui** -> per crear el directori
2. **cd ui** -> per entrar a aquest directori
3. **wget url** -> aquí pots afegir el link del que vols descarregar-te i ho instal·la amb el wget.
4. **ls** -> et mostra el que hi ha 
5. **tar xuf el-que-sigui** -> x: extrae, u: veus el que está extraient, f: fitxer
6. **ls -lh**
7. **./wireguard-ui --help** -> et mostra coses
8. **./wireguard-ui**
9. **netstat -tulpn** -> per veure si hi ha alguna cosa oberta

-------------------
- **firewall-cmd --list--all** -> et mosta una ''ip table''.. et diu els portas, rich rules,etc. *Amb el --list--all, solament t'ensenya el públic*
- **systemctl status firewall** -> per veure els firewalls
- **docker ps** -> es poden veure els ports que s'estiguin utilitzant
- **add port numero-port-/masc** -> per afegir un nou port.
    - *el docker el que fa és que tots el ports que creas ho obre en el teu host automaticament*

- **firewall -cmd --list--all-zones** -> lista de zones
- **firewall -cmd new zone nom-de-la-zona** -> et crea una zona
- **curl icanhazip.com** -> per veure en quina ip m'he connectat

- **firewall -cm --add-rich-rule = '** -> per afegir una regla nova, pots copiar la vella i canviar el que vulguis (com els ports, ips...)

Et crea una carpeta ''db'' on están les claves del servidor. Les pots editar i posar la clave pública/privada que vulguis.



# Servei que s'autoinici

Pots buscar directament en **github** -> **wireguard** -> **autostart wg daemon**. Això crea un path que miri si hi ha canvis en wg0, si n'hi ha, executa el servei (autostart).

Passos:

1. **cd /etc/systemd/systm**
2. **ls**
3. **nano wireguard-ui.service .**
    - Pots editar el fitxer que vulguis
