# Documentació feta 1 Desembre

Aviu hem parlat una mica sobre com funciona Isard per dins:

- Frontends (vue.js)
- Api (python/flask)
- Authentification (golang)
- Bases de dades (rethinkdb)
- Engine (python): hyp 1, hyp 2, hyp 3.
- Webapp (python/flask)
- VPN
- HAProxy




## Docker

- **systemctl status ...** -> per veure el status del servei
- **docker image ls** -> per veure la llista d'imatges
- **docker stop (start) nomdearxiu** -> per parar o arrancar l'arxiu
- **docker rm -f nomdearxiu** -> per forçar l'eliminació
- **docker compose** -> separa base da dades, apps i webs
- **docker compose -f nextcloud.yml up -d** -> per que l'arxiu estigui up
- **docker rmi #IDImagen** -> per esborrar una imatge
- **docker system prune --all** -> elimina tot el que no tinguis en funcionament en aquest instant
- **container_name** -> dins d'un arxiu, posar això per donar nom al container


## Docker Compose Setup

1. Crea un directori per el teu projecte:
- **mkdir composetest**
- **cd composetest**
2. Crea un arxiu amb el nom 'app.py' en el eu projecte i enganxa això:
`import time

import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)` 




## Git

|Comandes|Descrpció|
|---------|-----------|
|**git init + nomprojecte**|Crearà un nou repositori local|
|**git clone** | Copia un repositori
|- servidor remot: *git clone nomdelusuari@host:/path/to/repsitory*
|- repsitori local: *git clone/path/to/repositori*
|**git add temp.md** | Afegeix arxius|
|**git commit** | Crea una instantànea dels canvis i els guarda en el directori git
| - *git commit -m 'El missatge que vulguis'*
|**git config** | Per establir una configuració específica d'un usuari, com el seu email, nom, i tipus de format.
| - *--global user.email tuemail@ejemplo.com* | Li diu a git que utilitzarás aquest correu per tots el repositoris locals|
|**git status** | Mosta la llista dels arxius que s'han canviat juntament amb els que s'estan preparan.|
|**git push** | Envia confirmacions locals a la rama main|
| - *git push origin main* | Reemplaza main per la branca a la que vulguis enviar els canvis |
|**git checkout** | Crea branques i t'ajuda a navegar entre elles (descarta els teus canvis)|
| - *git checkout branch_name* | Per canviar d'una branca a una altre.|
|**git remote** | Et permeteix veure tots els repositoris remots|
| - *git remote -v* | Llistarà totes les conexions juntament amb els seus URLs|
| - *git remote addorigin (host-or-remoteURL)* | Conectar el repositori local a un servidor remot|
| - *git remote (nom-del-repositori)* | Esborra una conexió a un repositori remot|
|**git branch** | Llista, crea o esborra branques|
|- *git branch -d branch_name* | Esborrar una branca|
|**git pull** | Fusiona tots els canvis que s'han fet en el repositori remot amb el directori de treball local (descarrega el que hi hagi)|
|**git merge** | Fusiona una branca amb una altre|
| - *git merge branch_name*
|**git diff** | Per fer una llista de conflictes (o veure-la)|
|-*git diff --base file_name* | Conflictes amb respecte al arxiu base|
|-*git diff source_branch target_branch* | Conflictes que hi ha entre branques abans de fusionar-les|
|**git tag** | Marca commits específics|
|**git log** | Veure l'historial llistan certs detalls|
|**git reset** | Resetea l'index i el directori de treball a l'últim estat de confirmació|
|**git rm filename.md** | Esborra arxius del índex i del directori de treball|
|**git stash** | Guardará momentáneament els canvis que no estiguin llistos per ser confirmats.|
|-*git stash apply* | Per aplicar l'ho guardat|
|**git show** | Mosta informació sobre qualsevol objecte git|
|**git fetch** | Permet a l'usuari buscar tots els objectes d'un repositori remot que actualment no es troba en el directori de treball local|
|-*git fetch origin*
|




- **git fetch isard** ->
**git rebase isard/main** : per anar actualitzant per si hi han hagut canvis mentre treballes
- **git push origin --delete nombrebranca** -> esborrar la branca desde la web de git
- **git branch** -> **nombranca** -> esborrar en remot

## Git Squash

- **git rebase -i #idcommit** -> Per canviar un commit
- **git pull**
- **git log**
- **git reset --hard origin/main**

